//
//  JIPOI.h
//  JedAI
//
//  Created by Hezi Cohen on 3/2/18.
//  Copyright © 2018 ANAGOG ltd. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, JIPOIType) {
    JIPOITypeUnknown,
    JIPOITypeMall = 0,
    JIPOITypePoliceStation,
    JIPOITypeUniversity,
    JIPOITypeRailway,
    JIPOITypeHospital,
    JIPOITypeSchool,
    JIPOITypeParking,
    JIPOITypeAirport,
    JIPOITypeGolf,
    JIPOITypeFootball,
    JIPOITypeGym,
    JIPOITypeHotel,
    JIPOITypeDogPark,
    JIPOITypeWorship,
    JIPOITypeSports,
    JIPOITypeFuel
};

FOUNDATION_EXTERN JIPOIType JIPOITypeFromString(NSString *_Nullable string);
FOUNDATION_EXTERN NSString *_Nonnull JIStringFromPOIType(JIPOIType type);

@interface JIPOI : NSObject <NSCopying, NSCoding>
@property (nullable, nonatomic, copy) NSNumber *centerLat;
@property (nullable, nonatomic, copy) NSNumber *centerLng;
@property (nullable, nonatomic, copy) NSNumber *isDrivingPOI;
@property (nullable, nonatomic, copy) NSNumber *locationID;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, retain) NSArray<CLLocation *> *poly;
@property (nullable, nonatomic, copy) NSNumber *priority;
@property (nullable, nonatomic, copy) NSNumber *special;
@property (nullable, nonatomic, retain) NSObject *tags;
@property (nonatomic, assign) JIPOIType type;
@property (nullable, nonatomic, copy) NSString *typeString;
@property (nullable, nonatomic, copy) NSString *value;
//@property (nullable, nonatomic, retain) NSSet<MSDbRawVisitEvent *> *visit;

- (instancetype _Nonnull)initWithDictionary:(NSDictionary *_Nonnull)dict;
- (BOOL)isEqualToPOI:(JIPOI *_Nullable)jipoi;

@end
