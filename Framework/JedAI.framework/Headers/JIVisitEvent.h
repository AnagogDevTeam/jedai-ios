//
//  JIVisitEvent.h
//  JedAI
//
//  Created by Hezi Cohen on 3/2/18.
//  Copyright © 2018 ANAGOG ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, JIVisitType) {
    JIVisitTypeUnknown = 0,
    JIVisitTypeArrival,
    JIVisitTypeDeparture
};

@class JIPOI, CLVisit;

@interface JIVisitEvent : NSObject <NSCopying, NSCoding>

@property (nonatomic, assign) JIVisitType visitType;

@property (nonatomic, readonly, copy) NSDate *_Nullable arrivalDate;
@property (nonatomic, readonly, copy) NSDate *_Nullable departureDate;

@property (nonatomic) double latitude;
@property (nonatomic) double longitude;
@property (nonatomic) double accuracy;

@property (nonatomic) double poiConfidence;
@property (nonatomic, readonly, copy) JIPOI *_Nullable pointOfInterest;

- (instancetype _Nonnull)initWithVisit:(CLVisit *_Nonnull)visit pointOfInterest:(JIPOI *_Nonnull)pointOfInterest poiConfidence:(float)poiConfidence;

- (instancetype _Nonnull)initWithVisitType:(JIVisitType)visitType arrivalDate:(NSDate *_Nullable)arrivalDate departureDate:(NSDate *_Nullable)departureDate latitude:(double)latitude longitude:(double)longitude accuracy:(double)accuracy poiConfidence:(double)poiConfidence pointOfInterest:(JIPOI *_Nullable)pointOfInterest;

- (BOOL)isEqual:(id _Nullable)other;

- (BOOL)isEqualToEvent:(JIVisitEvent *_Nullable)event;

- (NSUInteger)hash;

- (id _Nonnull)copyWithZone:(nullable NSZone *)zone;
@end
