//
//  JedAI.h
//  JedAI
//
//  Created by Hezi Cohen on 3/1/18.
//  Copyright © 2018 ANAGOG ltd. All rights reserved.
//

#import "JIPOI.h"
#import "JIVisitEvent.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//! Project version number for JedAI.
FOUNDATION_EXPORT double JedAIVersionNumber;

//! Project version string for JedAI.
FOUNDATION_EXPORT const unsigned char JedAIVersionString[];

typedef void (^JINotificationBlock)(JIVisitEvent *event);

@interface JedAI : NSObject
+ (void)setup;
+ (void)start;
+ (void)stop;
#if TARGET_IPHONE_SIMULATOR
+ (void)setupWithSimulatedScenario:(NSDictionary *)scenario;
#endif

+ (void)registerForNotificationsUsingBlock:(JINotificationBlock)handler;

@end
